<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml"/>

    <xsl:template match="/animais">
        <animais>
            <xsl:apply-templates select="animal[ ./peso > 10 ]"/>
        </animais>
    </xsl:template>

    <xsl:template match="animal">
        <xsl:element name="animal">
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="animal/*">
        <xsl:element name="{name()}">
            <xsl:choose>
                <xsl:when test="name() = 'vacinas'" > 
                    <xsl:apply-templates/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="text()"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:element>
    </xsl:template>
    <xsl:template match="vacinas/vacina">
        <xsl:element name="{name()}">
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="vacina/*">
        <xsl:element name="{name()}">
            <xsl:value-of select="text()"/>
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>
