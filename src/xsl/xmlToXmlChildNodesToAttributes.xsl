<?xml version="1.0"  encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml"/>

    <xsl:template match="/animais">
        <animais>
            <xsl:apply-templates/>
        </animais>
    </xsl:template>
    <xsl:template match="animal" >
        <animal>
            <xsl:apply-templates select="*[name() != 'vacinas']"/>
        </animal>
    </xsl:template>
    <xsl:template match="*">
        <xsl:attribute name="{name()}">
          <xsl:value-of select="text()"/>
        </xsl:attribute>
    </xsl:template>
</xsl:stylesheet>

