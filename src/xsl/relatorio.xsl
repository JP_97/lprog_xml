<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:ns="http://www.dei.isep.ipp.pt/lprog" exclude-result-prefixes="ns">
    <xsl:output method="html"/>

    <xsl:template match="/">
        <html>
            <head>
                <title>LPROG</title>
            </head>
            <body bgcolor="#FFFFFF" text="#0066ff" link="#808080">
                <div align="justify">
                    <font face="Comic Sans MS">
                        <br/>
                        <br/>
                        <xsl:apply-templates select="ns:relatório/ns:páginaRosto/ns:logotipoDEI"/>
                  
                        <br/>
                        <br/>
                        <font size="5" color="#0066ff">
                            <b> 
                                <xsl:value-of select="ns:relatório/ns:páginaRosto/ns:tema"/>
                            </b>
                          
                        </font>
                     
                        <h1>
                            <font size="4" color="#000000">Relatório</font>
                        </h1>
                        
                        <font size="4" color="#000000">
                            <font size="4" color="#000000">
                                <xsl:value-of
                                    select="ns:relatório/ns:páginaRosto/ns:disciplina/ns:designação"
                                />
                            </font>
                            
                            <font size="4" color="#000000">  2017/2018</font>
                            <p/>
                            <font size="4" color="#000000">
                                <font color="#0066ff">Turma: </font>
                                <xsl:value-of select="ns:relatório/ns:páginaRosto/ns:turma"/>
                            </font>
                            <br/>
                            <p/>
                            <font size="4" color="#000000">
                                <font color="#0066ff">Autores:</font>
                                <br/>
                                <xsl:for-each select="ns:relatório/ns:páginaRosto/ns:autor">
                                    <xsl:value-of select="ns:nome"/> - <xsl:value-of
                                        select="ns:número"/>
                                    <br/>
                                </xsl:for-each>
                            </font>
                            <p/>
                            <font size="4" color="#000000">
                                <font color="#0066ff">Professores:</font>
                                <br/> AMD - T <br/> PSM - TP<br/> PDF - PL </font>
                            <p/>
                            <hr color="#808080"/>
                            <p/>
                            <font size="5" color="#0066ff">
                                <b>Índice</b>
                            </font>
                            <p/>
                            <font size="5" >
                                <a href="#heading1">Introdução</a>
                                <br/>
                                <a href="#heading2">Análise</a>
                                <br/>
                                <a href="#heading3">Linguagem</a>
                                <br/>
                                <a href="#heading4">Transformações</a>
                                <br/>
                                <a href="#heading5">Conclusão</a>
                                <br/>
                                <a href="#heading6">Referências</a>
                                <br/>
                                <a href="#heading7">Anexos</a>
                                <br/>
                            </font>
                            <p/>
                            <hr color="#808080"/>

                            <h1 id="heading1">
                                <font size="5" color="#0066ff">
                                    <b>Introdução</b>
                                </font>
                            </h1>
                            <font size="4" color="#000000">
                                <p align="justify">
                                    
                                    <xsl:for-each select="ns:relatório/ns:corpo/ns:introdução/ns:parágrafo">
                                        <xsl:apply-templates match="ns:relatório/ns:corpo/ns:introdução/ns:parágrafo"/>
                                        <br/>
                                    </xsl:for-each>
                                
                                    <xsl:for-each
                                        select="ns:relatório/ns:corpo/ns:introdução/ns:listaItems/ns:item"
                                    > - <xsl:value-of select="text()"/>
                                        <br/>
                                    </xsl:for-each>
                                    <br/>
                                </p>
                            </font>
                            <hr color="#808080"/>

                            <h1 id="heading2">
                                <font size="5" color="#0066ff">
                                    <b>Análise</b>
                                </font>
                            </h1>
                            <font size="4" color="#000000">
                                <p align="justify">
                                    <xsl:for-each select="ns:relatório/ns:corpo/ns:outrasSecções/ns:análise/ns:parágrafo">
                                        <xsl:apply-templates match="ns:relatório/ns:corpo/ns:outrasSecções/ns:análise/ns:parágrafo"/>
                                        <br/>
                                    </xsl:for-each>
                                    <br/>
                                    <xsl:for-each select="ns:relatório/ns:corpo/ns:outrasSecções/ns:análise/ns:listaItems/ns:item"> 
                                        - <xsl:value-of select="text()"/>
                                        <br/>
                                    </xsl:for-each>
                                    <br/>
                                </p>
                            </font>
                            <hr color="#808080"/>

                            <h1 id="heading3">
                                <font size="5" color="#0066ff">
                                    <b>Linguagem</b>
                                </font>
                            </h1>
                            <font size="4" color="#000000">
                                <p align="justify">
                                    <xsl:for-each select="ns:relatório/ns:corpo/ns:outrasSecções/ns:linguagem/ns:parágrafo">
                                        <xsl:apply-templates match="ns:relatório/ns:corpo/ns:outrasSecções/ns:linguagem/ns:parágrafo"/>
                                        <br/>
                                    </xsl:for-each>
                                
                                </p>
                         
                            </font>
                            <hr color="#808080"/>
                            <h1 id="heading4">
                                <font size="5" color="#0066ff">
                                    <b>Transformações</b>
                                </font>
                            </h1>
                            <font size="4" color="#000000">
                                <p align="justify">
                                    <xsl:for-each select="ns:relatório/ns:corpo/ns:outrasSecções/ns:transformações/ns:parágrafo">
                                        <xsl:apply-templates match="ns:relatório/ns:corpo/ns:outrasSecções/ns:transformações/ns:parágrafo"/>
                                        <br/>
                                    </xsl:for-each>
                                    
                                    <xsl:for-each
                                        select="ns:relatório/ns:corpo/ns:outrasSecções/ns:transformações/ns:listaItems/ns:item"
                                    > - <xsl:value-of select="text()"/>
                                        <br/>
                                    </xsl:for-each>
                                  
                                </p>
                                <br/>
                            </font>
                            <hr color="#808080"/>
                            <h1 id="heading5">
                                <font size="5" color="#0066ff">
                                    <b>Conclusão</b>
                                </font>
                            </h1>
                            <font size="4" color="#000000">
                                <p align="justify">
                                        <xsl:for-each select="ns:relatório/ns:corpo/ns:conclusão/ns:parágrafo">
                                        <xsl:apply-templates match="ns:relatório/ns:corpo/ns:conclusão/ns:parágrafo"/>
                                        <br/>
                                    </xsl:for-each>
                                    
                                  
                                </p>
                        
                            </font>
                            <hr color="#808080"/>
                            <p/>
                            <h1 id="heading6">
                                <font size="5" color="#0066ff">
                                    <b>Referências</b>
                                </font>
                            </h1>
                            <font size="3" color="#000000">
                                <p align="center">
                                    <xsl:for-each
                                        select="ns:relatório/ns:corpo/ns:referências/ns:refBibliográfica">
                                        <p>
                                            <xsl:value-of select="ns:título"/>; Autor <xsl:value-of
                                                select="ns:autor"/> (<xsl:value-of
                                                select="ns:dataPublicação"/>) </p>
                                    </xsl:for-each>
                                    <xsl:for-each
                                        select="ns:relatório/ns:corpo/ns:referências/ns:refWeb">
                                        <p>
                                            <xsl:value-of select="ns:descrição"/> - URL <a>
                                                <xsl:attribute name="href">
                                                    <xsl:value-of select="ns:URL"/>
                                                </xsl:attribute>
                                                <xsl:value-of select="ns:URL"/>
                                            </a>(<xsl:value-of select="ns:consultadoEm"/>)</p>
                                    </xsl:for-each>
                                </p>
                                <br/>
                            </font>
                            <hr color="#808080"/>
                            <p/>
                            <h1 id="heading7">
                                <font size="5" color="#0066ff">
                                    <b>Anexos</b>
                                </font>
                            </h1>
                            <font size="3" color="#000000">
                                <p align="center">
                                    <xsl:for-each select="ns:relatório/ns:anexos">
                                        <p>
                                            <xsl:value-of select="ns:parágrafo"/>
                                            <xsl:apply-templates select="ns:figura"/>
                                            <br/>
                                        </p>
                                    </xsl:for-each>
                                </p>
                                <br/>
                            </font>
                        </font>
                    </font>
                </div>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="ns:figura">
        <div style="text-size:12px">
            <img style="width:500px">
                <xsl:attribute name="src">
                    <xsl:value-of select="@src"/>
                </xsl:attribute>
            </img>
            <br/>
            <br/>
            <xsl:value-of select="@descrição"/>
        </div>
    </xsl:template>

    <xsl:template match="ns:logotipoDEI">
        
        <xsl:element name="img">
            <xsl:attribute name="src">
                <xsl:value-of select="."/>
            </xsl:attribute>
            <xsl:attribute name="style">
                <xsl:text>
                    width: 10%; height: auto;
                </xsl:text>
            </xsl:attribute>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="ns:parágrafo">
      
        <p align="justify">
            <xsl:value-of select="text()"/>
           
        </p>
      
    </xsl:template>


</xsl:stylesheet>
