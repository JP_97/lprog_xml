<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : newstylesheet.xsl
    Created on : 26 de Maio de 2018, 18:03
    Author     : Utilizador
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="text" indent="yes"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/animais">
        <xsl:apply-templates select="animal"/>
    </xsl:template>

    <xsl:template match="animal">
        <xsl:text>Animal:</xsl:text>
        <xsl:apply-templates select="tipoAnimal"/>
        <xsl:apply-templates select="nome"/>
        <xsl:apply-templates select="idade"/>
        <xsl:apply-templates select="sexo"/>
        <xsl:apply-templates select="vacinas"/>
    </xsl:template>
    
    <xsl:template match="tipoAnimal">
        <xsl:text>&#xD;&#xA;</xsl:text>
        <xsl:text> Tipo de animal: </xsl:text>
        <xsl:value-of select="."/>
        <xsl:text>&#xD;&#xA;</xsl:text>
    </xsl:template>
    <xsl:template match="nome">
        <xsl:text> Nome: </xsl:text>
        <xsl:value-of select="."/>
        <xsl:text>&#xD;&#xA;</xsl:text>
    </xsl:template>
    <xsl:template match="idade">
        <xsl:text> Idade: </xsl:text>
        <xsl:value-of select="."/>
        <xsl:text>&#xD;&#xA;</xsl:text>
    </xsl:template>
    <xsl:template match="sexo">
        <xsl:text> Sexo: </xsl:text>
        <xsl:value-of select="."/>
        <xsl:text>&#xD;&#xA;</xsl:text>
    </xsl:template>
    <xsl:template match="vacinas">
        <xsl:text>Vacinas: </xsl:text>
           <xsl:text>&#xD;&#xA;</xsl:text>
        <xsl:for-each select="./vacina"
        > - <xsl:value-of select="./nomeVacina"/>
          <xsl:text>&#xD;&#xA;</xsl:text>
        </xsl:for-each>
    </xsl:template>

</xsl:stylesheet>

<!-- tipo animal,nome,idade,sexo e peso-->