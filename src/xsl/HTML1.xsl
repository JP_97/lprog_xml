<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : HTML1.xsl
    Created on : 22 de Maio de 2018, 16:39
    Author     : 1161021
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/animais">
        <html>
            <head>
                <title>HTML1.xsl</title>
            </head>  
            <body style="background-color:powderblue;">
                <br/>
                <br/>
                <h1 align="center">Listagem dos animais do Adota Pets</h1>
                <br/>
                <br/>
                <table border="1" align="center">
                    <tr bgcolor="#33ccff">
                        <th style="text-align:left"> nome </th>
                        <th style="text-align:left"> tipoAnimal </th>
                        <th style="text-align:left"> raça </th>
                        <th style="text-align:left"> idade </th>
                        <th style="text-align:left"> sexo </th>
                        <th style="text-align:left"> peso </th>
                        <th style="text-align:left"> personalidade </th>
                        <th style="text-align:left"> box </th>
                        <th style="text-align:left"> tipoRação </th>
                        <th style="text-align:left"> exercício </th>
                
                    </tr>
                    <xsl:apply-templates/>
                </table>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="animal">
        <html>           
            <body>
                <tr>
                    <td>
                        <xsl:value-of select="nome"/>
                    </td>
                    <td>
                        <xsl:value-of select="tipoAnimal"/>
                    </td>
                    <td>
                        <xsl:value-of select="raça"/>
                    </td>
                    <td>
                        <xsl:value-of select="idade"/>
                    </td>
                    <td>
                        <xsl:value-of select="sexo"/>
                    </td>
                    <td>
                        <xsl:value-of select="peso"/>
                    </td>
                    
                    <td>
                        <xsl:value-of select="personalidade"/>
                    </td>
                    <td>
                        <xsl:value-of select="box"/>
                    </td>
                    <td>
                        <xsl:value-of select="tipoRacao"/>
                    </td>
                    <td>
                        <xsl:value-of select="exercicio"/>
                    </td>
                </tr>
            </body>         
        </html>
    </xsl:template>
</xsl:stylesheet>
