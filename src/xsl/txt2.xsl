<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : txt2.xsl
    Created on : 27 de Maio de 2018, 2:58
    Author     : Utilizador
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="text" indent="yes"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/animais">
        <xsl:text>ADOTA PETS&#xD;&#xA; </xsl:text>
          <xsl:text>Número de cães: </xsl:text>
            <xsl:value-of select="count(/animais/animal/tipoAnimal[.='cao'])"/>
              <xsl:text>&#xD;&#xA;</xsl:text>
            <xsl:text>Número de gatos: </xsl:text>
              <xsl:value-of select="count(/animais/animal/tipoAnimal[.='gato'])"/>
                <xsl:text>&#xD;&#xA;</xsl:text>
              <xsl:text>Outros: </xsl:text>
              <xsl:value-of select="count(/animais/animal/tipoAnimal[.='outro'])"/>
                  <xsl:text>&#xD;&#xA;</xsl:text>
        <xsl:text>Número de animais com vacinas: </xsl:text>
        <xsl:value-of select="count(/animais/animal/vacinas)"/>
         <xsl:text>&#xD;&#xA;</xsl:text>
        <xsl:text>Número de animais com ração normal: </xsl:text>
        <xsl:value-of select="count(/animais/animal/tipoRacao[.='normal'])"/>
        <xsl:text>&#xD;&#xA;</xsl:text>
    </xsl:template>
</xsl:stylesheet>
