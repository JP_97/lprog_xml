<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <xsl:template match="/">
        <html>
            <head>
                <title/>
            </head>
            <body style="background-color:powderblue;">
                <h1 align="center" >
                    <br/> 
                    <span style="color:white">
                        Listagem e contagem de animais por idade
                    </span>
                </h1>

                <!--Contar os anos -->
                <h3 align="center">  
                   
                    <span style="color:white">Nº de anos distintos: 
                        <xsl:value-of
                            select="count(/animais/animal/idade[not(. = ../preceding-sibling::animal/idade)])"
                        />
                    </span>
                </h3>
                <br/>
                <table align="center" border="1">
                    <tr bgcolor="#33ccff">
                        <th>Idade</th>
                        <th>Nome</th>
                        <th>Tipo de animal</th>
                        <th>Género</th>
                    </tr>
                    <!--Listar os anos -->
                    <xsl:apply-templates
                        select="/animais/animal/idade[not(. = ../preceding-sibling::animal/idade)]"
                    > 
                    </xsl:apply-templates>
                </table>

            </body>
        </html>
    </xsl:template>

    <xsl:template match="idade">
        <tr>
            <td bgcolor="#33ccff">
                <xsl:value-of select="."/>
            </td>
            <td>
                <xsl:for-each select="/animais/animal[idade = current()]">

                  
                    <xsl:value-of select="nome"/>
                    <br/>
                  
                </xsl:for-each>
            </td>
            <td>
                <xsl:for-each select="/animais/animal[idade = current()]">
                             
                    <xsl:apply-templates
                        select="tipoAnimal"/>
               
                </xsl:for-each>
            </td>
            <td>
                <xsl:for-each select="/animais/animal[idade = current()]">
                             
                    <xsl:apply-templates
                        select="sexo"/>
               
                </xsl:for-each>
            </td>
        </tr>
    </xsl:template>

    <xsl:template match="tipoAnimal">
        <span style="colo#339933">
            <xsl:value-of select="."/>
        </span>
        <br/>
    </xsl:template>
    <xsl:template match="sexo">
        <span style="colo#339933">
            <xsl:value-of select="."/>
        </span>
        <br/>
    </xsl:template>

</xsl:stylesheet>