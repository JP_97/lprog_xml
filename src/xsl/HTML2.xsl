<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : HTML2.xsl
    Created on : 23 de Maio de 2018, 14:40
    Author     : 1161021
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="animais">
        <html>
            <head>
                <title>CAES.xsl</title>
            </head>
           <body style="background-color:powderblue;">
                    <h1 align="center">Listagem dos cães do Adota Pets</h1>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th style="text-align:left"> nome </th>
                        <th style="text-align:left"> tipoAnimal </th>
                        <th style="text-align:left"> raça </th>
                        <th style="text-align:left"> idade </th>
                        <th style="text-align:left"> sexo </th>
                        <th style="text-align:left"> peso </th>
                        <th style="text-align:left"> personalidade </th>
                        <th style="text-align:left"> box </th>
                        <th style="text-align:left"> tipoRação </th>
                        <th style="text-align:left"> exercício </th>                                  
                    </tr>
                    <xsl:apply-templates select="./animal/tipoAnimal[.='cao']"/>
                </table>      
            </body>
        </html> 
         
    </xsl:template>
    
    <!--  <xsl:template match="tipoAnimal[.='cao']">  -->
    <xsl:template match="tipoAnimal">
        
        <html>           
            <body>
                <tr>
                    <td>
                        <xsl:value-of select="../nome"/>
                    </td>
                    <td>
                        <xsl:value-of select="../tipoAnimal"/>
                    </td>
                    <td>
                        <xsl:value-of select="../raça"/>
                    </td>
                    <td>
                        <xsl:value-of select="../idade"/>
                    </td>
                    <td>
                        <xsl:value-of select="../sexo"/>
                    </td>
                    <td>
                        <xsl:value-of select="../peso"/>
                    </td>
                    <td>
                        <xsl:value-of select="../personalidade"/>
                    </td>
                    <td>
                        <xsl:value-of select="../box"/>
                    </td>
                    <td>
                        <xsl:value-of select="../tipoRacao"/>
                    </td>
                    <td>
                        <xsl:value-of select="../exercicio"/>
                    </td>
                </tr>
            </body>         
        </html>
    </xsl:template>
</xsl:stylesheet>
